using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.SceneManagement;
using TMPro;

// Sets the script to be executed later than all default scripts
// This is helpful for UI, since other things may need to be initialized before setting the UI
[DefaultExecutionOrder(1000)]
public class MenuUIHandler : MonoBehaviour
{
    public TextMeshProUGUI bestScoreLabel;
    public TMP_InputField playerNameInput;

    private void PlayerNameUpdated()
    {
        if (PersistedData.Instance.playerName != playerNameInput.text.Trim() && playerNameInput.text.Trim() != "")
        {
            PersistedData.Instance.playerName = playerNameInput.text;
            PersistedData.Instance.bestScore = 0;
            PersistedData.Instance.Save();
        }
    }

    private void Start()
    {
        PersistedData.Instance.Load();
        bestScoreLabel.text = "Player: " + PersistedData.Instance.playerName + " - High Score: " + PersistedData.Instance.bestScore;
    }

    public void StartNew()
    {
        PlayerNameUpdated();
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void Exit()
    {
        PersistedData.Instance.Save();
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit(); // original code to quit Unity player
#endif
    }

}
